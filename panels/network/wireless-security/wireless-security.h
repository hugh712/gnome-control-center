/* -*- Mode: C; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* NetworkManager Applet -- allow user control over networking
 *
 * Dan Williams <dcbw@redhat.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Copyright 2007 - 2014 Red Hat, Inc.
 */

#pragma once

#include <gtk/gtk.h>
#include <NetworkManager.h>

G_BEGIN_DECLS

G_DECLARE_DERIVABLE_TYPE (WirelessSecurity, wireless_security, WIRELESS, SECURITY, GObject)

struct _WirelessSecurityClass {
	GObjectClass parent_class;

	void       (*add_to_size_group) (WirelessSecurity *sec, GtkSizeGroup *group);
	void       (*fill_connection)   (WirelessSecurity *sec, NMConnection *connection);
	gboolean   (*validate)          (WirelessSecurity *sec, GError **error);
	gboolean   (*adhoc_compatible)  (WirelessSecurity *sec);
	GtkWidget* (*get_widget)        (WirelessSecurity *sec);
};

GtkWidget *wireless_security_get_widget (WirelessSecurity *sec);

gboolean wireless_security_validate (WirelessSecurity *sec, GError **error);

void wireless_security_add_to_size_group (WirelessSecurity *sec,
                                          GtkSizeGroup *group);

void wireless_security_fill_connection (WirelessSecurity *sec,
                                        NMConnection *connection);

gboolean wireless_security_adhoc_compatible (WirelessSecurity *sec);

void wireless_security_set_username (WirelessSecurity *sec, const gchar *username);

const gchar *wireless_security_get_username (WirelessSecurity *sec);

void wireless_security_set_password (WirelessSecurity *sec, const gchar *password);

const gchar *wireless_security_get_password (WirelessSecurity *sec);

void wireless_security_set_always_ask (WirelessSecurity *sec, gboolean always_ask);

gboolean wireless_security_get_always_ask (WirelessSecurity *sec);

void wireless_security_set_show_password (WirelessSecurity *sec, gboolean show_password);

gboolean wireless_security_get_show_password (WirelessSecurity *sec);

void wireless_security_notify_changed (WirelessSecurity *sec);

G_END_DECLS
